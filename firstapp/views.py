from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def first(request):
	return HttpResponse("welcome to the first page")

def index(request):
	return render(request,"index.html")
